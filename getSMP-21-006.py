from __future__ import print_function
import hepdata_lib
from hepdata_lib import RootFileReader, Variable, Uncertainty, Table, Submission


submission = Submission()
#submission.read_abstract("abstract.txt")
#submission.add_link("Webpage with all figures and tables", "https://cms-results.web.cern.ch/cms-results/public-results/publications/B2G-16-029/") % TODO
#submission.add_link("arXiv", "http://arxiv.org/abs/arXiv:1802.09407") % TODO
#submission.add_record_id(1657397, "inspire") % TODO

uncertainties = ["JES", "JER","PU","PREF","PTHAT","MISS", "FAKE", "LUMI"] #// plus LUMI and stat that are symmetric

ptMax = ["200 < $p_{T1}$ < 400 GeV","400 < $p_{T1}$ < 800 GeV","$p_{T1}$ > 800"]
phi   = ["0 < $\Delta\Phi_{1,2}$ < 150^{\circ}", "150 < $\Delta\Phi_{1,2}$ < 170^{\circ}", "170 < $\Delta\Phi_{1,2}$ < 180^{\circ}"]
pTs   = ["$p_{T1}$","$p_{T2}$","$p_{T3}$","$p_{T4}$"]
pTs_name   = ["leading $p_{T}$ jet","subleading $p_{T}$ jet","third leading $p_{T}$ jet","fourth leading $p_{T}$ jet"]
Ptmin = [200,100,50,50]
Ptmax = [2000,2000,967,638]


def dataMult(xsec_loc, corr_loc):
    reader = RootFileReader("JetMult_forHEPdata.root")
    
    #CROSS SECTION
    for ipt in range(1,4):
        for iphi in range(1,4):
            unf = reader.read_hist_1d("stat_ptmax{}_phi{}".format(ipt,iphi),xlim = [1.5,7.5])
            Njets = Variable("Jet multiplicity", is_independent=True, is_binned=True, units="")
            Njets.values = unf["x_edges"]
            xsec = Variable("sigma", is_independent=False, is_binned=False, units="pb")
            xsec.values = unf["y"]
            
            stat = Uncertainty("stat.unc.", is_symmetric=True)
            stat.values = unf["dy"]

            xsec.add_uncertainty(stat)
            for uncertainty in uncertainties:
               
               if(uncertainty == "LUMI"):
                  unc = Uncertainty(uncertainty, is_symmetric=True)
                  value = reader.read_hist_1d(uncertainty+"_ptmax{}_phi{}".format(ipt,iphi),xlim=[1.5,7.5])
                  unc.values = value["y"]
               else:
                  unc = Uncertainty(uncertainty, is_symmetric=False)
                  up_values = reader.read_hist_1d(uncertainty+"up_ptmax{}_phi{}".format(ipt,iphi),xlim=[1.5,7.5])
                  dn_values = reader.read_hist_1d(uncertainty+"dn_ptmax{}_phi{}".format(ipt,iphi),xlim=[1.5,7.5])
                  unc.values = list(zip(up_values["y"],dn_values["y"]))

               xsec.add_uncertainty(unc)
             
            table = Table("Njets_xsec_pT1bin{}_phibin{}".format(ipt,iphi))
            table.description = "Jet multiplicity measured for a leading-pT jet with ({}) and for an azimuthal separation between the two leading jets of ({})".format(ptMax[ipt-1],phi[iphi-1])
            table.location = xsec_loc
            table.keywords["observables"] = ["sigma"]
            
            for var in [Njets,xsec]:
                table.add_variable(var)

            submission.add_table(table)
            
#    # CORRELATIONS
#    for ipt1 in range(1,4):
#       for ipt2 in range(1,4):
#          for iphi1 in range(1,4):
#             for iphi2 in range (1,4):
#                h = reader.read_hist_2d("Corr_ptmax{}{}_phi{}{}".format(ipt1,ipt2,iphi1,iphi2))
#                Njet1 = Variable("Jet multiplicity", is_independent=True, is_binned=True, units="") 
#                Njet1.values = h["x_edges"]
#                Njet2 = Variable("Jet multiplicity", is_independent=True, is_binned=True, units="") 
#                Njet2.values = h["y_edges"]
#                
#                corr = Variable("correlation", is_independent=False, is_binned=False, units="")
#                corr.values = h["z"]
#                
#                table = Table("JetMult_corr_ptmax{}{}_phi{}{}".format(ipt1,ipt2,iphi1,iphi2))
#                table.location = corr_loc
#                table.keywords["observables"] = ["correlations"]
#                table.description = "Correlation matrix at particle level for the measured jet multiplicity for $p_\\mathrm{{T1}}$ ({},{}) and $\Delta\phi_{{1,2}}$ ({},{}). It contains contributions from the data and from the limited-size MadGraph+Pythia8 sample. The z-axis cover a range from -1 to 1 indicating the anticorrelation and correlation.".format(ptMax[ipt1-1],ptMax[ipt2-1],phi[iphi1-1],phi[iphi2-1])
#                for var in [Njet1,Njet2,corr]:
#                   table.add_variable(var)
#                submission.add_table(table)

                 
def dataPtN(xsec_loc,corr_loc):
   reader = RootFileReader("PtN_forHepData.root");

   for iN in range(1,5):
      unf = reader.read_hist_1d("stat_Pt{}".format(iN),xlim=[Ptmin[iN-1],Ptmax[iN-1]])
      pTx = Variable("Jet pT ({})".format(pTs[iN-1]), is_independent=True, is_binned=True, units="GeV")
      pTx.values = unf["x_edges"]

      xsec = Variable("sigma", is_independent=False, is_binned=False, units="pb/GeV")
      xsec.values = unf["y"]
      
      stat = Uncertainty("stat.unc.", is_symmetric=True)
      stat.values = unf["dy"]

      xsec.add_uncertainty(stat)
      for uncertainty in uncertainties:
         
         if(uncertainty == "LUMI"):
            unc = Uncertainty(uncertainty, is_symmetric=True)
            value = reader.read_hist_1d(uncertainty+"_Pt{}".format(iN),xlim=[Ptmin[iN-1],Ptmax[iN-1]])
            unc.values = value["y"]
         else:
            unc = Uncertainty(uncertainty, is_symmetric=False)
            up_values = reader.read_hist_1d(uncertainty+"up_Pt{}".format(iN),xlim=[Ptmin[iN-1],Ptmax[iN-1]])
            dn_values = reader.read_hist_1d(uncertainty+"dn_Pt{}".format(iN),xlim=[Ptmin[iN-1],Ptmax[iN-1]])
            unc.values = list(zip(up_values["y"],dn_values["y"]))

         xsec.add_uncertainty(unc)
       
      table = Table("JetPt{}".format(iN))
      table.description = "Transverse momentum of the {} ({})".format(pTs_name[iN-1],pTs[iN-1])
      table.location = xsec_loc
      table.keywords["observables"] = ["sigma"]
            
      for var in [pTx,xsec]:
         table.add_variable(var)

      submission.add_table(table)
   

#   #CORRELATIONS
#   for iNx in range(1,5):
#      for iNy in range(1,5):
#         h = reader.read_hist_2d("Corr_pT{}{}".format(iNx,iNy))
#         pTx = Variable("{}".format(pTs[iNx-1]), is_independent=True, is_binned=True, units="GeV")
#         pTx.values = h["x_edges"]
#         pTy = Variable("{}".format(pTs[iNy-1]), is_independent=True, is_binned=True, units="GeV")
#         pTy.values = h["y_edges"]
#
#         corr = Variable("correlation")
#         corr = Variable("correlation", is_independent=False, is_binned=False, units="")
#         corr.values = h["z"]
# 
#         table = Table("Corr_pT{}{}".format(iNx,iNy))
#         table.description = "Correlation matrix for the particle-level $p_{{T}}$ measured for ({},{}). It contains contributions from the data and from the limited-size MadGraph+Pythia8 sample. The z-axis cover a range from -1 to 1 indicating the anticorrelation and correlation".format(pTs[iNx-1],pTs[iNy-1])
#         table.location = corr_loc
#         table.keywords["observables"] = ["correlations"]
#         
#         for var in [pTx,pTy,corr]:
#            table.add_variable(var)
#
#         submission.add_table(table)


def dataMultTUB(xsec_loc, corr_loc):
    reader = RootFileReader("JetMult_forHEPdata_TUnfoldbinning.root")
    
    #CROSS SECTION
    unf = reader.read_hist_1d("stat",xlim=[0.5,54.5])
    Njets = Variable("Jet multiplicity BinID", is_independent=True, is_binned=True, units="")
    Njets.values = unf["x_edges"]
    xsec = Variable("sigma", is_independent=False, is_binned=False, units="pb")
    xsec.values = unf["y"]
    stat = Uncertainty("stat.unc.", is_symmetric=True)
    stat.values = unf["dy"]

    xsec.add_uncertainty(stat)
    for uncertainty in uncertainties:
       if(uncertainty == "LUMI"):
          unc = Uncertainty(uncertainty, is_symmetric=True)
          value = reader.read_hist_1d(uncertainty, xlim=[0.5,54.5])
          unc.values = value["y"]
       else:
          unc = Uncertainty(uncertainty, is_symmetric=False)
          up_values = reader.read_hist_1d(uncertainty+"up",xlim = [0.5,54.5])
          dn_values = reader.read_hist_1d(uncertainty+"dn",xlim = [0.5,54.5])
          unc.values = list(zip(up_values["y"],dn_values["y"]))

       xsec.add_uncertainty(unc)
     
    table = Table("jet_multiplicity")
    table.description = "Jet multiplicity 3D distribution in TUnfold binning as indicated in the XML file provided as additional resource" 
    table.add_additional_resource("This XML file encodes the physical binning of the distribution.","JetMult_binning.xml", True )
    table.location = xsec_loc
    table.keywords["observables"] = ["sigma"]
    
    for var in [Njets,xsec]:
       table.add_variable(var)

    submission.add_table(table)
            
    # CORRELATIONS
    h = reader.read_hist_2d("corrMatrix")
    Njet1 = Variable("Jet multiplicity BinIDx", is_independent=True, is_binned=True, units="") 
    Njet1.values = h["x_edges"]
    Njet2 = Variable("Jet multiplicity BinIDy", is_independent=True, is_binned=True, units="") 
    Njet2.values = h["y_edges"]

    corr = Variable("correlation", is_independent=False, is_binned=False, units="")
    corr.values = h["z"]

    table = Table("correlations_jetmult")
    table.location = corr_loc
    table.add_additional_resource("This XML file encodes the physical binning of the x and y axes of the correlation matrix.","JetMult_binning.xml", True )
    table.keywords["observables"] = ["correlations"]
    table.description = "Correlation matrix at particle level for the measured jet multiplicity in TUnfold binning as indicated in the xml file"
    
    for var in [Njet1,Njet2,corr]:
        table.add_variable(var)
    submission.add_table(table)

def dataPtNTUB(xsec_loc, corr_loc):
    ptreader = RootFileReader("PtN_forHepData_TUnfoldbinning.root")
    
    #CROSS SECTION
    unfpt = ptreader.read_hist_1d("stat",xlim=[0.5,80.5])
    pTs = Variable("Jet pT BinID", is_independent=True, is_binned=True, units="")
    pTs.values = unfpt["x_edges"]
    ptxsec = Variable("sigma", is_independent=False, is_binned=False, units="")
    ptxsec.values = unfpt["y"]
    ptstat = Uncertainty("stat.unc.", is_symmetric=True)
    ptstat.values = unfpt["dy"]

    ptxsec.add_uncertainty(ptstat)
    for uncertainty in uncertainties:
       if(uncertainty == "LUMI"):
          unc = Uncertainty(uncertainty, is_symmetric=True)
          value = ptreader.read_hist_1d(uncertainty,xlim=[0.5,80.5])
          unc.values = value["y"]
       else:
          unc = Uncertainty(uncertainty, is_symmetric=False)
          up_values = ptreader.read_hist_1d(uncertainty+"up",xlim=[0.5,80.5])
          dn_values = ptreader.read_hist_1d(uncertainty+"dn",xlim=[0.5,80.5])
          unc.values = list(zip(up_values["y"],dn_values["y"]))

       ptxsec.add_uncertainty(unc)
     
    pttable = Table("jet_pt")
    pttable.add_additional_resource("This XML file encodes the physical $p_{T}$ binning of the distribution. Notice that still the normalization to the binwidth is needed to get the differential cross section.","PtN_binning.xml", True )
    pttable.description = "Jet pt 2D distribution in TUnfold binning as indicated in the xml file" 
    pttable.location = xsec_loc
    pttable.keywords["observables"] = ["sigma"]
    
    for var in [pTs,ptxsec]:
       pttable.add_variable(var)

    submission.add_table(pttable)
            
    # CORRELATIONS
    h_corr = ptreader.read_hist_2d("corrMatrix")
    Ptx = Variable("Jet pT BinIDx", is_independent=True, is_binned=True, units="") 
    Ptx.values = h_corr["x_edges"]
    Pty = Variable("Jet pT BinIDy", is_independent=True, is_binned=True, units="") 
    Pty.values = h_corr["y_edges"]

    ptcorr = Variable("correlation", is_independent=False, is_binned=False, units="")
    ptcorr.values = h_corr["z"]

    pttable = Table("correlations_pt")
    pttable.add_additional_resource("This XML file encodes the physical $p_{T}$ binning of the x and y axes of the correlation matrix.","PtN_binning.xml", True )
    pttable.location = corr_loc
    pttable.keywords["observables"] = ["correlations"]
    pttable.description = "Correlation matrix at particle level for the measured jet multiplicity in TUnfold binning as indicated in the xml file"
    
    for var in [Ptx,Pty,ptcorr]:
        pttable.add_variable(var)
    
    submission.add_table(pttable)






dataMult(xsec_loc = "Fig. 8 and Fig. 9", corr_loc = "Fig. 4") 
dataPtN(xsec_loc = "Fig. 10, Fig. 11 and Fig. 12", corr_loc = "Fig. 5")
dataMultTUB(xsec_loc = "Fig. 8 and Fig. 9", corr_loc = "Fig. 4") 
dataPtNTUB(xsec_loc = "Fig. 10, Fig. 11 and Fig. 12", corr_loc = "Fig. 5")

submission.create_files("SMP-21-006")
print("Done")
