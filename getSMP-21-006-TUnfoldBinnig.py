from __future__ import print_function
import hepdata_lib
from hepdata_lib import RootFileReader, Variable, Uncertainty, Table, Submission


submission = Submission()
#submission.read_abstract("abstract.txt")
#submission.add_link("Webpage with all figures and tables", "https://cms-results.web.cern.ch/cms-results/public-results/publications/B2G-16-029/") % TODO
#submission.add_link("arXiv", "http://arxiv.org/abs/arXiv:1802.09407") % TODO
#submission.add_record_id(1657397, "inspire") % TODO

uncertainties = ["JES", "JER","PU","PREF","PTHAT","MISS", "FAKE", "LUMI"] #// plus LUMI and stat that are symmetric

def dataMult(xsec_loc, corr_loc):
    reader = RootFileReader("JetMult_forHEPdata_TUnfoldbinning.root")
    
    #CROSS SECTION
    unf = reader.read_hist_1d("stat",xlim=[0.5,54.5])
    Njets = Variable("Jet multiplicity BinID", is_independent=True, is_binned=True, units="")
    Njets.values = unf["x_edges"]
    xsec = Variable("sigma", is_independent=False, is_binned=False, units="pb")
    xsec.values = unf["y"]
    stat = Uncertainty("stat.unc.", is_symmetric=True)
    stat.values = unf["dy"]

    xsec.add_uncertainty(stat)
    for uncertainty in uncertainties:
       if(uncertainty == "LUMI"):
          unc = Uncertainty(uncertainty, is_symmetric=True)
          value = reader.read_hist_1d(uncertainty, xlim=[0.5,54.5])
          unc.values = value["y"]
       else:
          unc = Uncertainty(uncertainty, is_symmetric=False)
          up_values = reader.read_hist_1d(uncertainty+"up",xlim = [0.5,54.5])
          dn_values = reader.read_hist_1d(uncertainty+"dn",xlim = [0.5,54.5])
          unc.values = list(zip(up_values["y"],dn_values["y"]))

       xsec.add_uncertainty(unc)
     
    table = Table("jet_multiplicity")
    table.description = "Jet multiplicity 3D distribution in TUnfold binning as indicated in the xml file" 
    table.location = xsec_loc
    table.keywords["observables"] = ["sigma"]
    
    for var in [Njets,xsec]:
       table.add_variable(var)

    submission.add_table(table)
            
    # CORRELATIONS
    h = reader.read_hist_2d("corrMatrix")
    Njet1 = Variable("Jet multiplicity BinIDx", is_independent=True, is_binned=True, units="") 
    Njet1.values = h["x_edges"]
    Njet2 = Variable("Jet multiplicity BinIDy", is_independent=True, is_binned=True, units="") 
    Njet2.values = h["y_edges"]

    corr = Variable("correlation", is_independent=False, is_binned=False, units="")
    corr.values = h["z"]

    table = Table("correlations_jetmult")
    table.location = corr_loc
    table.keywords["observables"] = ["correlations"]
    table.description = "Correlation matrix at particle level for the measured jet multiplicity in TUnfold binning as indicated in the xml file"
    
    for var in [Njet1,Njet2,corr]:
        table.add_variable(var)
    submission.add_table(table)

def dataPtN(xsec_loc, corr_loc):
    ptreader = RootFileReader("PtN_forHepData_TUnfoldbinning.root")
    
    #CROSS SECTION
    unfpt = ptreader.read_hist_1d("stat",xlim=[0.5,80.5])
    pTs = Variable("Jet pT BinID", is_independent=True, is_binned=True, units="")
    pTs.values = unfpt["x_edges"]
    ptxsec = Variable("sigma", is_independent=False, is_binned=False, units="")
    ptxsec.values = unfpt["y"]
    ptstat = Uncertainty("stat.unc.", is_symmetric=True)
    ptstat.values = unfpt["dy"]

    ptxsec.add_uncertainty(ptstat)
    for uncertainty in uncertainties:
       if(uncertainty == "LUMI"):
          unc = Uncertainty(uncertainty, is_symmetric=True)
          value = ptreader.read_hist_1d(uncertainty,xlim=[0.5,80.5])
          unc.values = value["y"]
       else:
          unc = Uncertainty(uncertainty, is_symmetric=False)
          up_values = ptreader.read_hist_1d(uncertainty+"up",xlim=[0.5,80.5])
          dn_values = ptreader.read_hist_1d(uncertainty+"dn",xlim=[0.5,80.5])
          unc.values = list(zip(up_values["y"],dn_values["y"]))

       ptxsec.add_uncertainty(unc)
     
    pttable = Table("jet_pt")
    pttable.description = "Jet pt 2D distribution in TUnfold binning as indicated in the xml file" 
    pttable.location = xsec_loc
    pttable.keywords["observables"] = ["sigma"]
    
    for var in [pTs,ptxsec]:
       pttable.add_variable(var)

    submission.add_table(pttable)
            
    # CORRELATIONS
    h_corr = ptreader.read_hist_2d("corrMatrix")
    Ptx = Variable("Jet pT BinIDx", is_independent=True, is_binned=True, units="") 
    Ptx.values = h_corr["x_edges"]
    Pty = Variable("Jet pT BinIDy", is_independent=True, is_binned=True, units="") 
    Pty.values = h_corr["y_edges"]

    ptcorr = Variable("correlation", is_independent=False, is_binned=False, units="")
    ptcorr.values = h_corr["z"]

    pttable = Table("correlations_pt")
    pttable.location = corr_loc
    pttable.keywords["observables"] = ["correlations"]
    pttable.description = "Correlation matrix at particle level for the measured jet multiplicity in TUnfold binning as indicated in the xml file"
    
    for var in [Ptx,Pty,ptcorr]:
        pttable.add_variable(var)
    
    submission.add_table(pttable)

dataMult(xsec_loc = "Fig. 8 and Fig. 9", corr_loc = "Fig. 4") 
dataPtN(xsec_loc = "Fig. 10, Fig. 11 and Fig. 12", corr_loc = "Fig. 5")

submission.create_files("SMP-21-006-TUnfoldBinning")
print("Done")
